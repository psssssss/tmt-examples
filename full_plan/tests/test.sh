#!/bin/bash

set -e

source ./lib.sh

BINARY="/bin/${1}"

# Tests

binary_exists ${BINARY}

check_fails
